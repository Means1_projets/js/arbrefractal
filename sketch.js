var angle
var nb = 0;
var slider;
var slider2;
var slider3;
var len;

function setup() {
  createCanvas(400, 400);
	slider = createSlider(0, TWO_PI*4, PI);
	slider2 = createSlider(0, 16, 1);
	slider3 = createSlider(5, 200, 100);
}

function draw() {
	background(51);
	stroke(255);
	angle = slider.value()/4;
	len = slider3.value();
	nb = slider2.value();
	translate(200, height);
	branch(len, nb);
}
function branch(len, lenb) {
	line(0,0,0,-len);
	translate(0, -len);
	if (lenb > 0) {
		push()
		rotate(angle);
		branch(len * 0.67, lenb-1);
		pop()
		push()
		rotate(-angle);
		branch(len * 0.67, lenb-1);
		pop()
	}
}
